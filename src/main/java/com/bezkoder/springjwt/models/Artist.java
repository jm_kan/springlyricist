package com.bezkoder.springjwt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table ( name = "artists")
public class Artist {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long artistId;
	private String malId;
	
	@OneToOne
	private User user;
	
	

	public Artist(User user) {
		
		this.user = user;
	}

	public Artist() {
		
	}

	public Artist(Long artistId, String malId, User user) {
		
		this.artistId = artistId;
		this.malId = malId;
		this.user = user;
	}

	
	
	public String getMalId() {
		return malId;
	}

	public void setMalId(String malId) {
		this.malId = malId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}

