package com.bezkoder.springjwt.security.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.RoleRepository;
import com.bezkoder.springjwt.repository.UserRepository;


@Service
public class UserService implements IUserService {

	
	@Autowired (required=true)
	private UserRepository userRepository;

	@Autowired (required=true)
	private RoleRepository roleRepository;

	

	@Override
	public User getUser(String username) {
		return  userRepository.findByUsername(username);
	}

	@Override
	public void deleteUser(Long userId) {
		 userRepository.deleteById(userId);
	}

	@Override
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUser() {
		return userRepository.findAll();
	}


}
