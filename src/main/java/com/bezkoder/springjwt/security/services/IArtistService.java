package com.bezkoder.springjwt.security.services;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.springjwt.models.Artist;
import com.bezkoder.springjwt.models.User;



public interface IArtistService  {
	
	
	
	public  List<Artist> getAllArtistFavByUser(User user) ;

	public void deleteArtistByUserAndMal_id(User user, String mal_id);
	
	
	

}
