package com.bezkoder.springjwt.security.services;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.Artist;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.ArtistRepository;
import com.bezkoder.springjwt.repository.UserRepository;


@Service
@Transactional
public class ArtistService implements IArtistService{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ArtistRepository artistRepository ;
	
	
	@Override
	public  List<Artist> getAllArtistFavByUser(User user){
		if(user!=null) {
			return user.getArtistsFav();
		}
		return null;
	}


	@Override
	public void deleteArtistByUserAndMal_id(User user, String mal_id) {
		this.artistRepository.deleteByUserAndMalId(user, mal_id);
	}
	


	
	
	

}
