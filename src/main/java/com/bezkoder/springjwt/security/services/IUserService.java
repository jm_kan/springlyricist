package com.bezkoder.springjwt.security.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.bezkoder.springjwt.models.User;

public interface IUserService {
	
		public User getUser(String username);

		public void deleteUser(Long userId);

		public User  updateUser(User user);

		public List<User> getAllUser();

	}
	
	

