package com.bezkoder.springjwt.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.bezkoder.springjwt.models.Artist;
import com.bezkoder.springjwt.models.User;


public interface ArtistRepository extends JpaRepository<Artist, Long>{
	
	
	
	public List<Artist> findByUser(User user);

	public void deleteByUserAndMalId(@Param("user") User user, @Param("mal_id") String mal_id);
}