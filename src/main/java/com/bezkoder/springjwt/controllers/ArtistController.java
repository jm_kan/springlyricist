package com.bezkoder.springjwt.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.models.Artist;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.ArtistRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import com.bezkoder.springjwt.security.services.IArtistService;
import com.bezkoder.springjwt.security.services.ArtistService;
import com.bezkoder.springjwt.security.services.UserService;

@RestController
@RequestMapping("/user/favoriteartist")
@CrossOrigin("*")
public class ArtistController {

	


		@Autowired
		private UserService userService;

		@Autowired
		private ArtistService artistService;

		@CrossOrigin
		@PostMapping("/add/{username}/{mal_id}")
		void addFavoriteArtists(@PathVariable("username") String username, @PathVariable("mal_id") String mal_id) {
			User user = this.userService.getUser(username);

			Artist artist = new Artist();
			artist.setUser(user);
			artist.setMalId(mal_id);

			user.getArtistsFav().add(artist);
			this.userService.updateUser(user);
		}

		@CrossOrigin
		@GetMapping("/all/{username}")
		public List<Artist> getFavArtists(@PathVariable("username") String username) {
			User user = new User();
			user = this.userService.getUser(username);
			return this.artistService.getAllArtistFavByUser(user);
		}

		@CrossOrigin
		@DeleteMapping("/delete/{username}/{mal_id}")
		void deleteFavoriteArtists(@PathVariable("username") String username, @PathVariable("mal_id") String mal_id) {
			User user = new User();
			user = this.userService.getUser(username);
			this.artistService.deleteArtistByUserAndMal_id(user, mal_id);
		}
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
